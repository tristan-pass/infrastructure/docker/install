#!/usr/bin/env bash
# 清理
sudo echo "清理当前宿主机内存"
sudo echo 1 > /proc/sys/vm/drop_caches
sudo echo 2 > /proc/sys/vm/drop_caches
sudo echo 3 > /proc/sys/vm/drop_caches


# 检测
sudo echo "查看系统版本"
sudo cat /etc/redhat-release

sudo echo "查看内核"
sudo uname -sr


# 配置环境变量实现适配
sudo echo "确定环境变量"
export dns_server=192.168.200.10
export network_name=ens160
export docker_version=18.06.3


# 修改主机配置
sudo echo "修改dns"
sudo nmcli con mod ${network_name} ipv4.dns "${dns_server}"
sudo nmcli con up ${network_name}

yum install -y net-tools
echo "修改主机名称为ip名称"
sudo hostnamectl set-hostname `ifconfig -a|grep inet|grep -v 127.0.0.1|grep -v inet6 | awk '{print $2}' | tr -d "addr:"`

echo "关闭selinux"
sudo sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config

echo "修改时区为上海"
sudo ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime

echo "修改系统语言环境"
sudo echo 'LANG="en_US.UTF-8"' >> /etc/profile;source /etc/profile

echo "同步时间"
sudo yum install -y ntp
sudo ntpdate pool.ntp.org

echo "kernel性能调优:"
echo "1、修复可能出现的网络问题"
echo "2、修改最大进程数"
sudo cat >> /etc/sysctl.conf<<EOF
net.ipv4.ip_forward=1
net.bridge.bridge-nf-call-iptables=1
net.ipv4.neigh.default.gc_thresh1=4096
net.ipv4.neigh.default.gc_thresh2=6144
net.ipv4.neigh.default.gc_thresh3=8192
kernel.pid_max=1000000
EOF

sudo systemctl restart network
sudo sysctl -p

echo "关闭防火墙"
sudo firewall-cmd --state
sudo systemctl stop firewalld.service
sudo systemctl disable firewalld.service


#安装docker
echo "安装docker"
echo "卸载旧版本Docker软件"
sudo yum remove -y docker \
              docker-client \
              docker-client-latest \
              docker-common \
              docker-latest \
              docker-latest-logrotate \
              docker-logrotate \
              docker-selinux \
              docker-engine-selinux \
              docker-engine \
              container*


echo "1、安装必要的一些系统工具"
sudo yum remove docker docker-client docker-client-latest \
    docker-common docker-latest docker-latest-logrotate \
    docker-logrotate docker-engine -y;
sudo yum update -y;
sudo yum install -y yum-utils device-mapper-persistent-data \
    lvm2 bash-completion;
echo "2、添加软件源信息"
sudo yum-config-manager --add-repo \
    http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo;
echo "3、更新并安装 Docker-CE"
sudo yum makecache all;
version=$(yum list docker-ce.x86_64 --showduplicates | sort -r|grep ${docker_version}|awk '{print $2}');
sudo yum -y install --setopt=obsoletes=0 docker-ce-${version} docker-ce-selinux-${version};


#配置docker
echo "配置docker aliyun 镜像加速器"
sudo tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://q4jtpmzm.mirror.aliyuncs.com"]
}
EOF

#启动&启用docker
echo "启动docker进程"
sudo systemctl start docker

echo "运行docker开机自启"
sudo systemctl enable docker

echo "运行一个hello-world镜像"
sudo docker run --name helloworld hello-world

echo "删除hello-world镜像"
sudo docker rm  helloworld
sudo docker rmi hello-world
